extern crate freetype as ft;
extern crate unicode_normalization;
extern crate image;

use std::path::Path;
use unicode_normalization::UnicodeNormalization;

fn draw_bitmap(bitmap: ft::Bitmap, min_x: usize, min_y: usize) -> Vec<Vec<u8>> {
    let mut figure: Vec<Vec<u8>> = Vec::new();
    let mut q = 0;
    let w = bitmap.width() as usize;
    let x_max = min_x + w;
    let y_max = min_y + bitmap.rows() as usize;
    let mut index = 0;
    println!("{}, {}", w, bitmap.rows());
    println!("{:?},  length: {}", bitmap.buffer(), bitmap.buffer().len());
    for y in 0 .. bitmap.rows() {
        figure.push(Vec::new());
        for x in 0 .. w {
            let bit = bitmap.buffer()[index / 8] & (1 << (7 - (index % 8)));
            let val = if bit > 0 { 1 } else { 0 };
            figure.last_mut().unwrap().push(val);
            index += 1;
        }
        index = (index / 16) * 16 + 16;
    }
    figure
}

fn draw_character(character: usize, face: &ft::Face, start_x: usize, start_y: usize, canvas: &mut Vec<Vec<u8>>) -> usize {
    face.load_char(character, ft::face::RENDER | ft::face::MONOCHROME).unwrap();

    let glyph = face.glyph();
    let x_offset = glyph.bitmap_left() as usize;
    let y_offset = glyph.bitmap_top() as usize;
    let figure = draw_bitmap(glyph.bitmap(), x_offset, y_offset);
    let mut y = start_y;
    for row in figure {
        if y >= canvas.len() {
            canvas.push(Vec::new());
        }
        let mut this_x = x_offset;
        for col in row {
            canvas[y].push(col);
        }
        y += 1;
    }
    glyph.linear_hori_advance() as usize
}

fn draw_string(msg: &str, face: &ft::Face, start_x: usize, start_y: usize, mut canvas: Vec<Vec<u8>>) -> Vec<Vec<u8>>{
    let mut x = start_x;
    let y = start_y;
    for c in msg.chars() {
        x += draw_character(c as usize, &face, x, y, &mut canvas);
    }
    canvas
}

            

fn main() {
    let ref mut args = std::env::args();

    if args.len() != 3 {
        let exe = args.next().unwrap();
        println!("Usage: {} font character", exe);
        return
    }

    let ref font = args.nth(1).unwrap();
    let message = args.next().unwrap();
    let library = ft::Library::init().unwrap();
    let face = library.new_face(font, 0).unwrap();

    face.set_char_size(40 * 64, 0, 100, 0).unwrap();

    let mut canvas = Vec::new();
    let canvas = draw_string(&message, &face, 0, 0, canvas);
    for row in canvas {
        for cell in row {
            print!("{}",
                   match  cell {
                       p if p == 0 => " ",
                       _ => "+",
                   }
                   );
        }
        println!("");
    }
}
